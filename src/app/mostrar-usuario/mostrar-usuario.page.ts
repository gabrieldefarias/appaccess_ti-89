import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-mostrar-usuario',
  templateUrl: './mostrar-usuario.page.html',
  styleUrls: ['./mostrar-usuario.page.scss'],
})
export class MostrarUsuarioPage implements OnInit {

  constructor(private actRoute: ActivatedRoute) { }
  id: string = "";
  nome: string = "";
  usuario: string = "";
  nivel: string = "";

  ngOnInit() {
    this.actRoute.params.subscribe((dadosdarota: any)=>{
      this.id = dadosdarota.id;
      this.nome = dadosdarota.nome;
      this.usuario = dadosdarota.usuario;
      this.nivel = dadosdarota.nivel;
    });
  }
}
