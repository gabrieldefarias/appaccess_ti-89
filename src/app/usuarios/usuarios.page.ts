import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';
import { PostService } from 'src/services/post.service';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.page.html',
  styleUrls: ['./usuarios.page.scss'],
})
export class UsuariosPage implements OnInit {
  nome: string="";
  limite: number=10;
  inicial: number=0;
  usuarios: any=[]; /* Define uma matriz vazia */
  constructor( // Declarações
    private router: Router,
    private service: PostService,
    private toastCtrl: ToastController,
    private alertCtrl: AlertController
  ) { }

  ngOnInit() {
    /*  // Usar este formulario para testar as rotas
    let user ={
      id: "1",
      nome: "Gabriel",
      usuario: "gabriel",
      senha_original: "123456",
      nivel: "admin"
    };
    this.usuarios.push(user);
    */
  }
  ionViewWillEnter(){
    this.usuarios = [];
    this.inicial = 0;
    this.carregar();
  } // Garante que a tela sempre exiba dados atualizados
  addUsuario(){
    this.router.navigate(['add-usuario']);
  } // Final do metodo add-usuario
  carregar(){
    return new Promise(ret =>{
      this.usuarios = [];
      let dados = {
        requisicao: "listar",
        nome: this.nome,
        limit: this.limite,
        start: this.inicial
      };
      this.service.dadosApi(dados, 'api.php').subscribe(data =>{
        if(data['result']=='0'){
          this.ionViewWillEnter();
        }else{
          for(let usuario of data['result']){
            this.usuarios.push(usuario);
          }
        }
        ret(true);
      });
    });
  } // Final do metodo carregar
  mostrar(id, nome, usuario, nivel){
    this.router.navigate(['mostrar-usuario/'+id+'/'+nome+'/'+usuario+'/'+nivel]);
  } // Final do metodo mostrar
  editar(id, nome, usuario, senha, nivel){
    this.router.navigate(['add-usuario/'+id+'/'+nome+'/'+usuario+'/'+senha+'/'+nivel]);
  } // Final do metodo editar
  excluir(id){
    return new Promise(()=>{
      let dados = {
        requisicao: 'excluir',
        id: id
      };
      this.service.dadosApi(dados, "api.php").subscribe(async data =>{
        if(data['success']){
          const toast = await this.toastCtrl.create({
            message: data['msg'],
            duration: 5000,
            position: 'middle',
            color: 'danger'
          });
          toast.present()
        }
        this.ionViewWillEnter(); // Data
      });
    })
  } // Final do metodo excluir
  async alertarExcluao(id, usuario){
    const alert = await this.alertCtrl.create({
      header: 'Confirmação da exclusão do usuário' + usuario,
      buttons: [{
        text: 'Cancelar', role: 'cancel', cssClass: 'ligth',
        handler:()=>{
          // Caso usuário clique em cancelar
        }
        },{
        text: 'OK',
        handler:()=>{
          this.excluir(id);
        }
      }]
    });
    alert.present();
  }
  ativar(id, ativo){
    if(ativo == '1') ativo='0';
    else ativo='1';
    return new Promise(()=>{
      let dados = {
        requisicao: 'ativar',
        id: id,
        ativo: ativo
      };
      this.service.dadosApi(dados, "api.php").subscribe(async data =>{
        this.ionViewWillEnter();
      });
    })
  } // Fim do método ativar
}
