import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()
export class PostService {
  server: string = 'http://localhost/apiaccess/';

  constructor(private http: HttpClient) { }

  dadosApi(dados: any, nomeApi: string){
    const httpOptions = {
      headers: new HttpHeaders({'Content-Type':'application/json'})
    }
    let url = this.server + nomeApi;
    return this.http.post(url, JSON.stringify(dados), httpOptions).map(res => res)
  }
}
/* Pagina API

<?php
    include_once('conexao.php');

    // Variável que recebe o conteúdo da requisição do app decodificando-a
    $postjson = json_decode(file_get_contents('php://input', true), true);
    if($postjson['requisicao']=='add'){
        $query = $pdo -> prepare(
            "INSERT INTO usuarios SET nome = :nome, usuario = :usuario, senha = :senha,
            senha_original = :senha_original, nivel = :nivel, ativo = 1"
        );
        $query -> bindValue(":nome", $postjson['nome']);
        $query -> bindValue(":usuario", $postjson['usuario']);
        $query -> bindValue(":senha", md5($postjson['senha']));
        $query -> bindValue(":senha_original", $postjson['senha']);
        $query -> bindValue(":nivel", $postjson['nivel']);
        $query -> execute();
        $id = $pdo -> lastInsertId();
        if($query){
            $result = json_encode(array('success'=>true, 'id'=>$id));
        }else{
            $result = json_encode(array('success'=>false, 'msg'=>'Falha ao inserir os dados'));
        }
        echo $result;
    } // Final da requisição ADD
    elseif($postjson['requisicao']=='listar'){
        if($postjson['nome']==''){
            $query = $pdo->query(
                "SELECT * FROM usuarios order by id desc limit $postjson[start],
                $postjson[limit]"
            );
        }else{
            $busca = $postjson['nome'].'%';
            $query = $pdo->query(
                "SELECT * FROM usuarios where nome like '$busca' or usuario like '$busca'
                order by id desc limit $postjson[start], $postjson[limit]"
            );
        }
        $res = $query->fetchAll(PDO::FETCH_ASSOC);
        for($i=0; $i < count($res); $i++){
            foreach($res[$i] as $key => $value){ }
            $dados[] = array(
                'id'=> $res[$i]['id'],
                'nome'=> $res[$i]['nome'],
                'usuario'=> $res[$i]['usuario'],
                'senha'=> $res[$i]['senha'],
                'senha_original'=> $res[$i]['senha_original'],
                'nivel'=> $res[$i]['nivel'],
                'ativo'=> $res[$i]['ativo']
            );
        }
        if(count($res) > 0){
            $result = json_encode(array('success'=>true, 'result'=>$dados));
        }else{
            $result = json_encode(array(
                'success'=>false, 'result'=>'0'
            ));
        }
        echo $result;
    } // Final da requisição LISTAR
    elseif($postjson['requisicao']=='editar'){
        $query = $pdo->prepare(
            "UPDATE usuarios SET nome = :nome, usuario = :usuario, senha = :senha,
            senha_original = :senha_original, nivel = :nivel where id = :id"
        ); // inject SQL
        $query -> bindValue(":nome", $postjson['nome']);
        $query -> bindValue(":usuario", $postjson['usuario']);
        $query -> bindValue(":senha", md5($postjson['senha']));
        $query -> bindValue(":senha_original", $postjson['senha']);
        $query -> bindValue(":nivel", $postjson['nivel']);
        $query -> bindValue(":id", $postjson['id']);
        $query -> execute();

        if($query){
            $result = json_encode(array('success'=>true, 'msg'=>'Ocorreu tudo bem.'));
        }else{
            $result = json_encode(array(
                'success'=>false, 'msg'=>'Dados Incorretos para Usuário na API TI-89'
            ));
        }
        echo $result;
    } // Final da requisição EDITAR
    elseif($postjson['requisicao']=='atualizar'){
        //
    } // Final da requisição ATUALIZAR
    elseif($postjson['requisicao']=='excluir'){
        //$query = $pdo->query("DELETE FROM usuarios WHERE id = $postjson[id]");
        $query = $pdo->query("UPDATE usuarios SET ativo = 0 WHERE id = $postjson[id]");
        if($query){
            $result = json_encode(array('success'=>true, 'msg'=>"Usuário excluído"));
        }else{
            $result = json_encode(array('success'=>false));
        }
        echo $result;
    } // Final da requisição EXCLUIR
    elseif($postjson['requisicao']=='login'){
        $query = $pdo->query(
            "SELECT * FROM usuarios where usuario = '$postjson[usuario]'
            and senha = md5('$postjson[senha]') and ativo = 1"
        );
        $res = $query->fetchAll(PDO::FETCH_ASSOC);
        for ($i=0; $i < count($res); $i++){
            foreach ($res[$i] as $key => $value){ }
            $dados = array(
                'id'=> $res[$i]['id'],
                'nome'=> $res[$i]['nome'],
                'usuario'=> $res[$i]['usuario'],
                'senha'=> $res[$i]['senha'],
                'senha_original'=> $res[$i]['senha_original'],
                'nivel'=> $res[$i]['nivel']
            );
        }
        if(count($res) > 0){
            $result = json_encode(array('success'=>true, 'result'=>$dados));
        }else{
            $result = json_encode(array(
                'success'=>false, 'msg'=>'Dados incorretos ou usuário pode estar inativo.'
            ));
        }
        echo $result;
    } // Final da requisição LOGIN
    elseif($postjson['requisicao']=='ativar'){
        //$query = $pdo->query("DELETE FROM usuarios WHERE id = $postjson[id]");
        $query = $pdo->query("UPDATE usuarios SET ativo = $postjson[ativo] WHERE id = $postjson[id]");
        if($query){
            $result = json_encode(array('success'=>true, 'msg'=>"Usuário alterado"));
        }else{
            $result = json_encode(array('success'=>false));
        }
        echo $result;
    } // Final da requisição ATIVAR
?>
*/

/* Pagina conexao

<?php
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    header('Content-Type: application/json; charset=utf-8');  

    // dados de acesso do servidor local
    $banco = 'accessDb';
    $host = 'localhost';
    $user = 'root';
    $pass = '';

    try{
        $pdo = new PDO("mysql:dbname = $banco; host = $host", $user, $pass);
    }catch (Exception $e){
        echo "Falha ao conectar ao banco ". $banco .". Verifique! - " . $e;
    }
?>
*/